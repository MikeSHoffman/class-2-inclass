let row1 = ['-', 'O', '-'];
let row2 = ['-', 'X', 'O'];
let row3 = ['X', '-', 'X'];
let board = [row1, row2, row3];

//board:
console.log(board[0].join(' '));
console.log(board[1].join(' '));
console.log(board[2].join(' '));

console.log("set upper right to O:");

row1[2] = 'O';

// board:

console.log(board[0].join(' '));
console.log(board[1].join(' '));
console.log(board[2].join(' '));
