let me = {
    firstName: "Michael",
    lastName: "Hoffman",
    monthsCoding: 36,
    mom: {
       firstName: "Rebecca",
       lastName: "Trujillo",
       monthsCoding: 0
    },
    dad: {
      firstName: "Darrell",
      lastName: "Hoffman",
      monthsCoding: 0
    }
};

console.log("dad's first name: ", me.dad.firstName);
console.log("mom's months coding: ", me.mom.monthsCoding);
