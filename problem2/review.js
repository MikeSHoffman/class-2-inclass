const assignmentDate = '1/21/2019';

// Convert that string to a Date instance

const dateObj = new Date(assignmentDate);

console.log("dateObj: ", dateObj);

// Create dueDate which is 7 days after assignmentDate
// Create the due date as a Date instance (new Date).
const dueDate = new Date(dateObj.getFullYear(), dateObj.getMonth(), dateObj.getDate() + 7);
console.log("dueDate: ", dueDate);

const monthNames = ["January", "February", "March", "April", "May", "June", 
                    "July", "August", "September", "October", "November", "December"];

// Use dueDate values to create an HTML time tag as a string in the format:
// '<time datetime="YYYY-MM-DD">Month day, year</time>'
// for example:
// '<time datetime="2018-01-14">January 14, 2018</time>'

const htmlTimeTag = `<time datetime="${dueDate.getFullYear()}-${(dueDate.getMonth()+1).toString().padStart(2, '0')}-${dueDate.getDate().toString().padStart(2, '0')}">${monthNames[dueDate.getMonth()]} ${dueDate.getDate()}, ${dueDate.getFullYear()}</time>`;

// log this value using console.log
console.log("htmlTimeTag: ", htmlTimeTag);

// notes:

// Months and days in datetime attribute must have leading zeroes, see string padStart method 
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/padStart

// Month is referenced as an index (0 is first) in Date
